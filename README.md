## Gradient - hexo theme for video bookmarks.

## Features

- Responsive
- Featured image
- Charts
- Twitter widget.

## External libraries used

- [Bootstrap 4 with Popper.js, and jQuery](https://getbootstrap.com/)
- [Font awesome](https://fontawesome.com/)

## Installation

Install the theme by using:

`$ git clone https://quocvu@bitbucket.org/quocvu/video-bookmarks-hexo-theme.git themes/gradient`

Then update your blog's main _config.yml to set the theme to gradient.

## Configuration
The configuration is done in `themes/gradient/_config.yml`


## Changelog

1.0
- Initial release.
